
<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ): ?>



<section class="hero">
	<div class="container">
		<div class="row y-center">

					<div class="column text-column col-6">
						<div class="content">
							<h1>Resources</h1>
							<p>​Browse our selection of free resources including conference presentations, SBAC test updates, and more. Have a specific question? Our <a href="/category/ask-an-expert/">Ask the UCLA Curtis Center blog</a> allows users to submit questions about mathematics education to our team of mathematicians and educators. </p>
						


							
	                	</div>
					</div>
				
						<div class="column col-6 image-column">
							<div class="img-wrap">
							<img src="/wp-content/uploads/2019/02/curtis-hero_library_1.png" style="max-width:850px;" />
								
							</div>
						</div>
		</div>
	</div>
</section>


<section class="resources-block">
	<div class="resources">
		<div class="container">
			<h2 class="text-center">Resource Library</h2>
			<div class="resource-list">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php $thumbnail = get_the_post_thumbnail_url();  ?>
					<div class="resource-item">
						<div class="module">
							<h3 class="headline"><span style="background-color: #ffc72f ;"></span><?php the_title(); ?></h3>
							<!-- <a href="<?php //the_permalink(); ?>" class="resource-photo" style="background-image: url(<?php //echo $thumbnail; ?>);">
								
							</a> -->
							<div class="resource-copy">					
								<div><?php the_excerpt(); ?></div>
							</div>
							<a href="<?php the_permalink(); ?>">Read More <span>&rarr;</span></a>
						</div>
					</div>
				<?php endwhile ?>
			</div>
		</div>
	</div>
</section>


<?php else: ?>
<h2>No posts to display</h2>	
<?php endif; ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>