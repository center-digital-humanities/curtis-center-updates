<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>


	<section>
		<div class="container">
			<div class="row x-center">
				<div class="column col-12">
					<h2>Sorry, page not found</h2>
				</div>
			</div>
		</div>
	</section>


<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>