<div class="header">
	<header class="header-mobile">
		<div class="container">
			<div class="module">
				<div class="brand">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
					</a>
				</div>
				<div class="navburger">
					<div class="buns">
						<span class="patty"></span>
						<span class="patty"></span>
						<span class="patty"></span>
					</div>
				</div>
			</div>
		</div>
	</header>
    <div class="header-wrapper" >
        <div class="menu-close"></div>
        <header class="header-main-mobile">
            <div class="container">
                <div class="module">
                    <div class="sub-pages-nav">
                        <nav class="small-only">
                            <?php wp_nav_menu( array( 'theme_location' => 'mobile-nav') ); ?>
                        </nav>
                    </div>
                </div>           
            </div>
        </header>
    </div>
    
<?php if(has_post_thumbnail()){ ?>
    <style>
        main {
            padding-top: 40px;
        }
        header.header-main .brand > a > img {
            width: 170px;
            height: auto;
        }
        header.header-main {
           background-color: transparent;
           border-bottom: none;
            text-shadow: 0px 0px 3px grey, 0px 0px 5px #000000;
        }
        .header {
            position: relative;
        }
        .super-header {
            background-color: transparent;
            -webkit-transition: all 0.3s;
            -o-transition: all 0.3s;
            transition: all 0.3s;
        }
        .top.hero {
            height: 100%;
            min-height: 860px;
        }
        .title-row {
            margin-top: 10rem;
        }
        header.header-main .sub-pages-nav ul li {
            text-transform: uppercase;
            font-weight: 600;
        }
        .sub-pages-nav ul li a {
            color: #fff;
            text-shadow: 0px 0px 3px grey, 0px 0px 5px #000000;
            
        }
        header.header-main .sub-pages-nav nav ul li ul {
            background: #000;
            display: inline;
        }
        
    </style>
<?php }?>
<div class="top <?php if(has_post_thumbnail()){ ?>hero<?php } ?>" <?php if(has_post_thumbnail()){ ?>style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"<?php } ?>>
    
<?php //the_post_thumbnail( 'content-width' ); ?>
	<!--// <div class="menu-close"></div> //-->
	<div class="super-header" id="super-header">
		<div class="container">
			<div class="module">
				<div class="super-nav">
				<div class="nav-main">
					<nav>
						<?php wp_nav_menu( array( 'theme_location' => 'super-nav') ); ?>
					</nav>
				</div>
				<div class="search-form-wrap"><?php get_search_form(); ?></div>
				</div>
			</div>
		</div>
	</div>
	<header class="header-main">
		<div class="container">
			<div class="module">
				<div class="brand">                    
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
                        <?php if(has_post_thumbnail()){ ?>
                        <img src="<?php the_field('logo', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>"> <?php } else { ?><img src="<?php the_field('logo_contrast', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>"> <?php } ?>
					</a>                    
				</div>
				<div class="sub-pages-nav">
					<nav class="large-only">
						<?php wp_nav_menu( array( 'theme_location' => 'main-menu') ); ?>
					</nav>
                    <!--//
					<nav class="small-only">
						<?php// wp_nav_menu( array( 'theme_location' => 'mobile-nav') ); ?>
					</nav>
                    //-->
				</div>
			</div>           
            <?php if(has_post_thumbnail()){ ?>
                <div class="title-row text-center white_txt add_dropshadow<?php if( !empty(get_sub_field('txt_align'))){ the_sub_field('txt_align'); } ?>">
                    <div class="content">
                    <?php if (get_the_title()): ?>
                        <h1 class="headline <?php if( !empty(get_sub_field('headline_size'))){ the_sub_field('headline_size'); } ?>"><?php the_title(); ?></h1>                        
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <span class="brief">
                                <?php the_content(); ?>
                            </span>
                        <?php endwhile; endif; ?>
                    <?php endif ?>
                    </div>
                </div>
            <?php } ?>
		</div>
	</header>
</div>
</div>
<main>
