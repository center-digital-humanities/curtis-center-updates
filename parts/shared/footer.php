<?php 

    $git_title = get_field('git_title', 'option');
    $git_content = get_field('git_copy', 'option');     
    $git_link = get_field('git_button', 'option');  
    $git_secondlink = get_field('git_second_button', 'option');
    $git_link_txt = get_field('git_button_txt', 'option');  
    $git_secondlink_txt = get_field('git_second_button_txt', 'option');
?>	


<?php if ($git_title || $git_content ) { ?>	
<section id="get_in_touch" class="two-col-block <?php if( !empty(get_field('git_text_color', 'option'))){ the_field('git_text_color', 'option'); } ?><?php if( !empty(get_field('git_background_image', 'option'))){ echo ' '. add_dropshadow; } ?> <?php if( !empty(get_field('git_bg_color', 'option'))){ the_field('git_bg_color', 'option'); } ?>" <?php if( !empty(get_field('git_background_image', 'option')) || get_field('git_no_pad', 'option')){ ?>style="background-image: url(<?php the_field('git_background_image', 'option'); ?>); <?php if( get_field('git_no_pad', 'option') ): ?>padding: 0; <?php endif; ?>

"<?php } ?>>
	<div class="container">

		<div class="row x-center">

			<div class="column text-column col-10 text-center <?php if( !empty(get_field('git_txt_align', 'option'))){ the_field('git_txt_align', 'option'); } ?>">
				<div class="content">
				<?php if ($git_title): ?>
					<h2 class="headline big_title <?php if( !empty(get_field('git_headline_size', 'option'))){ the_field('git_headline_size', 'option'); } ?>"><?php echo $git_title; ?></h2>
				<?php endif ?>
					<?php echo $git_content; ?>

					<?php if( $git_link ): ?>
	                       <a href="<?php echo $git_link['url']; ?>" class="btn rectangle" target="<?php echo $git_link['target']; ?>" title="<?php if( $git_link_txt ){ echo $git_link_txt; } else { echo $git_link['title']; } ?>"><?php if( $git_link_txt ){ echo $git_link_txt; } else { echo $git_link['title']; } ?></a>
                	<?php endif; ?>

					<?php if( $git_secondlink ): ?>
	                       <a href="<?php echo $git_secondlink['url']; ?>" class="btn rectangle" target="<?php echo $git_secondlink['target']; ?>" title="<?php if( $git_secondlink_txt ){ echo $git_secondlink_txt; } else { echo $git_secondlink['title']; } ?>"><?php if( $git_secondlink_txt ){ echo $git_secondlink_txt; } else { echo $git_secondlink['title']; } ?></a>
                	<?php endif; ?>
				
            	</div>
			</div>
			
		</div>
	</div>
</section>
<?php } ?>
</main>
<footer>
	<div class="footer-main">
		<div class="container">
			<div class="module">
				<div class="row">
					<div class="column brand">
						<a href="/" title="<?php bloginfo( 'name' ); ?>">
							<img src="<?php the_field('logo_contrast', 'option'); ?>" alt="<?php bloginfo( 'name' ); ?>">
						</a>
					</div>
                    
				<div class="copyright">
					<p class="vcard">
						<?php if (get_field('department_name', 'option')) { ?>	
						<?php the_field('department_name', 'option'); ?> is part of the <a href="https://www.math.ucla.edu/">UCLA Department of Mathematics</a> within <a href="http://www.college.ucla.edu/">UCLA College</a>.<br />
						<?php } ?>
						<span class="adr">
							<?php if (get_field('main_office_location', 'option')) { ?>	
							<span class="street-address"><?php the_field('main_office_location', 'option'); ?></span> <span class="divider">|</span> <span class="locality"><?php the_field('city_state', 'option'); ?></span> <span class="postal-code"><?php the_field('zip_code', 'option'); ?></span>
							<?php } ?>
							<?php if (get_field('phone_number', 'option')) { ?>	
							<span class="divider">|</span> <span class="tel"><strong>P:</strong> <span class="value"><?php the_field('phone_number', 'option'); ?></span></span>
							<?php } ?>
							<?php if (get_field('fax_number', 'option')) { ?>	
							<span class="divider">|</span> <span class="fax"><strong>F:</strong> <?php the_field('fax_number', 'option'); ?>
							<?php } ?>
							<?php if (get_field('dept_email_address', 'option')) {
							$department_email = antispambot(get_field('dept_email_address', 'option')); ?>
							<span class="divider">|</span> <span class="email"><strong>E:</strong> <a href="mailto:<?php echo $department_email; ?>"><?php echo $department_email; ?></a></span>
							<?php } ?>
						</span>
						<br />
						University of California &copy; <?php echo date('Y'); ?> UC Regents
					</p>
					<?php if(get_field('facebook', 'option') || get_field('twitter', 'option') || get_field('instagram', 'option') || get_field('contact_us', 'option')) { ?>
						<nav role="navigation" aria-label="Social Navigation" class="mobile-social-nav">
							<ul class="social-links">
							<?php if(get_field('facebook', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('facebook', 'option'); ?>"><span class="fab fa-facebook" aria-label="Like us on Facebook"></span></a></li>
							<?php } if(get_field('twitter', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('twitter', 'option'); ?>"><span class="fab fa-twitter" aria-label="Follow us on Twitter"></span></a></li>
							<?php } if(get_field('instagram', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('instagram', 'option'); ?>"><span class="fab fa-instagram" aria-label="Follow us on Instagram"></span></a></li>
							<?php } if(get_field('contact_us', 'option')) { ?>
								<li class="icon"><a href="<?php the_field('contact_us', 'option'); ?>"><span class="fas fa-envelope" aria-label="Contact us"></span></a></li>
							<?php } ?>
							</ul>
						</nav>
					<?php } ?>
					<!--//a href="http://www.ucla.edu" class="university-logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo-white.svg" alt="UCLA" /></a //-->
                    

					<?php if( have_rows('social_media_accounts', 'option') ): ?>
					<div class="column social">
						    <ul>
						    <?php while( have_rows('social_media_accounts', 'option') ): the_row(); ?>
						    	<?php  
						    		$url = get_sub_field('url');
						    		$name = get_sub_field('name');
						    		$img = get_sub_field('icon');
						    	?>
						        <li>
						        	<a href="<?php echo $url; ?>" title="<?php echo $name; ?>">
						        		<img src="<?php echo $img; ?>" alt="<?php echo $name; ?>">
						        	</a>
						        </li>
						    <?php endwhile; ?>
						    </ul>
					</div>
					<?php endif; ?>
				</div>
                    
                    

					<? if( have_rows('footer_nav', 'option') ): ?>
						<? while( have_rows('footer_nav', 'option') ): the_row(); ?>

						<div class="column col-2">

							<div class="nav-title"><strong><?php echo the_sub_field('column_title'); ?></strong></div>
							<? if( have_rows('footer_column') ): ?>
								<ul>
									<? while( have_rows('footer_column') ): the_row(); ?>
										<li><a href="<? the_sub_field('url'); ?>"><? the_sub_field('text'); ?></a></li>
									<? endwhile ?>
								</ul>
							<? endif ?> 
						</div>

						<? endwhile ?>
					<? endif ?>

				</div>
			</div>
		</div>
	</div>
<!-- 	<div class="footer-sub">
		<div class="container">
			<div class="module">
				<div class="row x-center">
					<div class="column col-3">
						<p>© Copyright 2018 by Yantriks</p>
					</div>
					<div class="column col-4">
						<ul>
							<li><a href="#">Terms of Use</a></li>
							<li><a href="#">Privacy Statement</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div> -->
</footer>