<!--Start Flex Content -->
<?php $k = 0; while(the_flexible_field("add_content_row")):  $k++ ?>
	<?php if(get_row_layout() == "hero"): ?>
		<!-- HERO -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_hero' ) ); ?>

	<?php elseif(get_row_layout() == "single_column"): ?>
		<!-- SINGLE COLUMN -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_single-column' ) ); ?>

	<?php elseif(get_row_layout() == "two_column"): ?>
		<!-- TWO COLUMNS -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_two-column' ) ); ?>

	<?php elseif(get_row_layout() == "team_members"): ?>
		<!-- TEAM MEMBERS -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_team-members' ) ); ?>

	<?php elseif(get_row_layout() == "virtual_councelor"): ?>
		<!-- VIRTUAL COUNCELOR -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_virtual-councelor' ) ); ?>

	<?php elseif(get_row_layout() == "contact_form_block"): ?>
		<!-- CONTACT FORM BLOCK -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_contact-form' ) ); ?>

	<?php elseif(get_row_layout() == "resources_block"): ?>
		<!-- RESOURCES BLOCK -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_resources-block' ) ); ?>

	<?php elseif(get_row_layout() == "tabs"): ?>
		<!-- TABS -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_tabs' ) ); ?>


	<?php elseif(get_row_layout() == "logo_grid"): ?>
		<!-- LOGOS -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_logo-grid' ) ); ?>

    <?php elseif(get_row_layout() == "logo_slider"): ?>
		<!-- LOGOS Slider -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_logo-col-slider' ) ); ?>

	<?php elseif(get_row_layout() == "facts_grid"): ?>
		<!-- FAST FACTS -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_facts-grid' ) ); ?>

	<?php elseif(get_row_layout() == "two_column_slider"): ?>
		<!-- TWO COLUMN SLIDER -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_two-col-slider' ) ); ?>

	<?php elseif(get_row_layout() == "latest_resources"): ?>
		<!-- TWO COLUMN SLIDER -->
		<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-parts/_latest-resources' ) ); ?>

	<?php endif; wp_reset_postdata(); ?>
<?php endwhile;  ?>

