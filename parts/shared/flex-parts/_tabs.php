	<section class="tabs-block">
		<div class="container">
			<div class="module">
				<h2 class="text-center"><?php the_sub_field('section_title') ?></h2>
				<div class="tabs">
					<div class="tabs-nav">
						<ul>
							<?php $slide_number = 0; ?>
							<?php while ( have_rows("tabs") ) : the_row(); ?>
								<?php $slide_number++;  ?>
								<li data-slide="<?php echo $slide_number; ?>"><?php the_sub_field('tab_title') ?></li>
							<?php endwhile ?>
						</ul>
					</div>
					<div class="tabs-content">
						<?php while ( have_rows("tabs") ) : the_row(); ?>
							<div>
							<div class="row y-center">
								<?php $image = get_sub_field('image'); ?>
								<?php $imageSize = get_sub_field('image_size'); ?>
								<?php if ($image): ?>
									<div class="column col-6 image-column">
										<div class="img-wrap">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="max-width:<?php echo $imageSize ?>;" />
											
										</div>
									</div>
								<?php endif ?>
								<div class="column text-column col-6">
									<div class="content">
										<div class="tab-content">
											<?php $tabTitle = get_sub_field('tab_title'); ?>
											<?php if ($tabTitle): ?>
												<h3 class="slide-title-mobile"><?php echo $tabTitle; ?></h3>
											<?php endif ?>
											
											<?php the_sub_field('copy') ?>
										</div>
									</div>
								</div>
							</div>
							</div>
						<?php endwhile ?>
					</div>
				</div>
			</div>
		</div>
	</section>