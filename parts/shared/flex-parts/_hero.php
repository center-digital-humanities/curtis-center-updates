<section class="hero" style="background-image: url(<?php the_sub_field('background_image'); ?>)">
	<div class="container">
		<div class="row y-center">

			<?php while ( have_rows("columns") ) : the_row(); ?>
				<?php 
				$columnType = get_sub_field('column_type');
				$link = get_sub_field('button');
				?>	
				<?php if ( $columnType == "text" ): ?>
					<div class="column text-column col-6">
						<div class="content">
							<?php the_sub_field('copy'); ?>
							
							<?php if( $link ): ?>
		                    	<a href="<?php echo $link['url']; ?>" class="btn" target="<?php echo $link['target']; ?>" title="<?php echo $link['title']; ?>"><?php echo $link['title']; ?></a>

		                	<?php endif; ?>
	                	</div>
					</div>
				<?php endif ?>

				<?php if ( $columnType == "image" ): ?>
					<?php $image = get_sub_field('image'); ?>
					<?php $imageSize = get_sub_field('image_size'); ?>
					<?php if ($image): ?>
						<div class="column col-6 image-column">
							<div class="img-wrap">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="max-width:<?php echo $imageSize ?>;" />
								
							</div>
						</div>
					<?php endif ?>
				<?php endif ?>

			<?php endwhile ?>
			
		</div>
	</div>
</section>