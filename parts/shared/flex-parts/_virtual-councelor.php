<section class="team_members-block <?php the_sub_field('section_color_theme'); ?>">
	<div class="team-members">
		<div class="container">
			<h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
			<div class="member-list">
				<?php while ( have_rows("card") ) : the_row(); ?>
					<?php 
						$featured_image = get_sub_field('featured_image');
						$title = get_sub_field('title');
						$copy = get_sub_field('copy');
						$link = get_sub_field('button');
						$link_ = get_sub_field('button_other');
					?>
					<div class="team-member vc">
						<div class="module">
							<div class="member-photo">
								<img src="<?php echo $featured_image['url']; ?>" alt="<?php echo $featured_image['alt']; ?>" />
							</div>
							<div class="member-copy">
								<div class="member-title"><?php echo $title; ?></div>
								<div class="member-bio"><?php echo $copy; ?></div>
								<?php if( $link ): ?>
			                    	<a href="<?php echo $link['url']; ?>" class="btn_alt" target="<?php echo $link['target']; ?>" title="<?php echo $link['title']; ?>"><?php echo $link['title']; ?></a>

			                	<?php endif; ?>
			                	<?php if( $link_ ): ?>
			                    	<a href="<?php echo $link_['url']; ?>" class="btn_alt" target="<?php echo $link_['target']; ?>" title="<?php echo $link_['title']; ?>"><?php echo $link_['title']; ?></a>

			                	<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endwhile ?>
			</div>
		</div>
	</div>
</section>