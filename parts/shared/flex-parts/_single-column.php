<?php  

?>

<section class="two-col-block <?php if( !empty(get_sub_field('text_color'))){ the_sub_field('text_color'); } ?><?php if( !empty(get_sub_field('background_image'))){ echo ' '. add_dropshadow; } ?> <?php if( !empty(get_sub_field('bg_color'))){ the_sub_field('bg_color'); } ?>" <?php if( !empty(get_sub_field('background_image')) || get_sub_field('no_pad')){ ?>style="background-image: url(<?php the_sub_field('background_image'); ?>); <?php if( get_sub_field('no_pad') ): ?>padding: 0; <?php endif; ?>

"<?php } ?>>
	<div class="container">

		<div class="row x-center">

			<div class="column text-column col-10 <?php if( !empty(get_sub_field('txt_align'))){ the_sub_field('txt_align'); } ?>">
				<div class="content">
				<?php if (get_sub_field('headline')): ?>
					<h2 class="headline <?php if( !empty(get_sub_field('headline_size'))){ the_sub_field('headline_size'); } ?>"><?php the_sub_field('headline'); ?></h2>
				<?php endif ?>
					<?php the_sub_field('copy'); ?>

					<?php 
					   $link = get_sub_field('button');
					?>

					<?php if( $link ): ?>
	                       <a href="<?php echo $link['url']; ?>" class="btn" target="<?php echo $link['target']; ?>" title="<?php echo $link['title']; ?>"><?php echo $link['title']; ?></a>
                	<?php endif; ?>
				
            	</div>
			</div>
			
		</div>
	</div>
</section>