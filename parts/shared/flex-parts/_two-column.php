<?php  

?>
    
<section class="two-col-block <?php if( !empty(get_sub_field('text_color'))){ the_sub_field('text_color'); } ?><?php if( !empty(get_sub_field('background_image'))){ echo ' '. add_dropshadow; } ?> <?php if( !empty(get_sub_field('bg_color'))){ the_sub_field('bg_color'); } ?>" <?php if( !empty(get_sub_field('background_image')) || get_sub_field('no_pad')){ ?>style="background-image: url(<?php the_sub_field('background_image'); ?>); <?php if( get_sub_field('no_pad') ): ?>padding: 0; <?php endif; ?>

"<?php } ?>>
	<div class="container">

		<div class="row <?php if( get_sub_field('center_align') ): ?>y-center<?php endif; ?>">

			<?php while ( have_rows("columns") ) : the_row(); ?>
				<?php 
				$columnType = get_sub_field('column_type');
				$link = get_sub_field('button');
				?>	
				<?php if ( $columnType == "text" ): ?>
					<div class="column text-column col-6">
						<div class="content">
						<?php if (get_sub_field('headline')): ?>
							<h3 class="headline"><?php the_sub_field('headline'); ?></h3>
						<?php endif ?>
						<?php the_sub_field('copy'); ?>
						
						<?php if( $link ): ?>
	                    	<a href="<?php echo $link['url']; ?>" class="btn" target="<?php echo $link['target']; ?>" title="<?php echo $link['title']; ?>"><?php echo $link['title']; ?></a>

	                	<?php endif; ?>
	                	</div>
					</div>
				<?php endif ?>

				<?php if ( $columnType == "image" ): ?>
					<?php $image = get_sub_field('image'); ?>
					<?php $imageSize = get_sub_field('image_size'); ?>
					<?php if ($image): ?>
						<div class="column col-6 image-column">
							<div class="img-wrap">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" style="max-width:<?php echo $imageSize ?>;" />
								
							</div>
						</div>
					<?php endif ?>
				<?php endif ?>

			<?php endwhile ?>
			
		</div>
	</div>
</section>