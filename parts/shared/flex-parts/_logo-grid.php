<?php 
    $change_layout = get_sub_field('change_layout');
    $circle_crop = get_sub_field('img_circle_crop');
?>
<section class="logo-grid <?php if( !empty(get_sub_field('logo_bg_color'))){ the_sub_field('logo_bg_color'); } ?>">
   
	<div class="container">
		<h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
		<ul class="logo-list <?php if( !empty($change_layout)){ echo $change_layout; } ?>">
			<?php while ( have_rows("logos") ) : the_row(); ?>
				<?php 
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$icons = get_sub_field('icons');
					$ext_link = get_sub_field('link');
					$page_link = get_sub_field('page_link');
					$brief_desc = get_sub_field('brief_description');            
                    $link_type =  get_sub_field('link_type');            
                    $img_select =  get_sub_field('image_selection'); 
                    $fa_select =  get_sub_field('font_awesome_type');
                   
                    //$link = ' ';
            
                    //echo esc_attr($link_type['value']);
            
                  //  $link_type_val = esc_attr($link_type['value']);
            //echo $link_type_val;
            
                    if ($link_type == 'show_pg'): 
                        // Display the Page Link; 
                        $link = $page_link;
                    elseif ($link_type == 'show_url'): 
                        // Display the External Link; 
                        $link = $ext_link;
                    endif;
				?>
            
                    <li>
                        
				
                <?php if ($img_select == 'show_icons' && !empty('$icons')): ?>
                    <figure class="icons"><i class="<?php echo $fa_select; ?> fa-<?php echo $icons; ?>"></i></figure>
				<?php endif; ?>
                
                <?php if ($img_select == 'show_imgs' && !empty('$image')): ?>
                        
                    <?php if($image) {

                        if( !empty($image) ): 
                        // vars
                        $url = $imageo['url'];
                        $image_title = $image['title'];
                        // thumbnail
                        $size = 'article-mid-thumb';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];
                    endif; ?>
                    <?php } ?>
                        
				    <figure class="img_section"><img src="<?php echo $thumb; ?>" alt="A photo of  <?php echo $image_title; ?>" <?php if($circle_crop == 'circle_crop'){ ?>class="circle_crop"<?php } ?> /></figure>
				<?php endif; ?>
                    <div class="content">
                        <h4>
                        <?php if ($link): ?>
                            <a href="<?php echo $link; ?>">
                        <?php endif; ?>
                            <?php // Displaying the title
                                echo $title;
                            ?>
                                
                        <?php if ($link): ?>
                            </a>
                        <?php endif; ?>
                        </h4>
                        <?php if ($brief_desc): ?>                        
                            <p>
                                <?php                                    
                                    $content = $brief_desc;
                                    if($change_layout == 'display_columns'):   
                                        //$limit = get_field('word_limit');
                                        $limit = 15;
                                        $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                        echo $trimmed_content;
                                    else:
                                        echo $content;
                                    endif;
                                ?>
                            </p>
                        <?php endif; ?>
                    </div>
				    </li>            
			<?php endwhile ?>
		</ul>
	</div>
</section>