<section class="team_members-block <?php the_sub_field('section_color_theme'); ?>">
    <div class="team-members">
        <div class="container">
            <h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
            <div class="member-list">
                <?php 
                    $team_circle_crop = get_sub_field('img_circle_crop'); 
                    $change_bg_color = get_sub_field('change_popup_bg_color');
                    $opacity_bg_color = get_sub_field('bg_opacity');
                ?>
                
                
                
                <style type="text/css">
                    .lity {
                            background-color: <?php echo $change_bg_color . $opacity_bg_color; ?> !important;
                        }
                
                </style>
                        
                <?php while ( have_rows("team_members") ) : the_row(); ?>
                    <?php 
                        $photo = get_sub_field('photo');
                        $name = get_sub_field('name');
                        $title = get_sub_field('title');
                        $bio = get_sub_field('bio');
                        $linkedin = get_sub_field('linkedin');
                        $email = get_sub_field('email');
                    ?>
                <a href="#<?php echo preg_replace('/\W+/','',strtolower(strip_tags($name))); ?>" data-lity>
                    <div class="team-member">
                        <div class="module">
							<?php if($photo) {
    
								if( !empty($photo) ): 
								// vars
								$url = $photo['url'];
								$photo_title = $photo['title'];
								// thumbnail
								$size = 'article-mid-thumb';
								$thumb = $photo['sizes'][ $size ];
								$width = $photo['sizes'][ $size . '-width' ];
								$height = $photo['sizes'][ $size . '-height' ];
							endif; ?>
                            <div class="member-photo">
                               <!-- <i class="fas fa-long-arrow-alt-right"></i> -->
                                <img src="<?php echo $thumb; ?>" alt="A photo of  <?php echo $photo_title; ?>" <?php if($team_circle_crop == 'circle_crop'){ ?>class="circle_crop"<?php } ?> />
                            </div>
							<?php } ?>
                            <div class="member-copy">
                                <div class="member-name"><?php echo $name; ?></div>
                                <div class="member-title"><?php echo $title; ?></div>                            
                                <div class="member-bio"><?php //echo $bio; ?></div>

                                <div id="<?php echo preg_replace('/\W+/','',strtolower(strip_tags($name))); ?>" class="lity-hide">
                                    <div class="member-photo">
                                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php echo $photo_title; ?>" <?php if($team_circle_crop == 'circle_crop'){ ?>class="circle_crop"<?php } ?> />
                                        <?php if(!empty($email)){ ?>
                                        <span class="email">
                                            <a href="mailto:<?php echo $email; ?>" class="btn_alt"><?php echo $email; ?></a>
                                        </span>
                                        <?php } ?>
                                    </div>
                                    <div class="member-content">
                                        <h1><?php echo $name; ?></h1>
                                        <h3><?php echo $title; ?></h3>
                                        <?php echo $bio; ?>
                                    </div>
                                </div>
                                <!--// a href="#<?php echo preg_replace('/\W+/','',strtolower(strip_tags($name))); ?>" data-lity>View Bio</a //-->


                            </div>
                        </div>
                    </div>
                </a>
                <?php endwhile ?>
            </div>
        </div>
    </div>
</section>