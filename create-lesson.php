<?php 
/* Template Name: Create a Lesson */ 
?>


<?php  
if ( is_user_logged_in() || current_user_can('publish_posts') ) { // Execute code if user is logged in
    acf_form_head();
    wp_deregister_style( 'wp-admin' );
}
?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section>
	<div class="container">
		<div class="row x-center">

			

			<div class="column text-column col-8">
				<div class="content">
					<h2><?php the_title(); ?></h2>
					<div class="lesson-form">
						<?php
						if ( ! ( is_user_logged_in() || current_user_can('publish_posts') ) ) {
						    echo '<p>You must be a registered author to create a lesson.</p>';
						} else {
						     acf_form(array(
						         'post_id' => 'new_post',
						         'field_groups' => array(468), // Used ID of the field groups here.
						         'post_title' => true, // This will show the title filed
						         'brief_lesson_summary' => true, // This will show the BRIEF LESSON SUMMARY
						         'parent_version' => true,
						         //'post_content' => true, // This will show the content field 
						         'form' => true,
						         'new_post' => array(
						             'post_type' => 'masterclass',
						             'post_status' => 'publish' // You may use other post statuses like draft, private etc.
						         ),
						         'return' => '%post_url%',
						         'submit_value' => 'Publish Lesson',
						     ));
						}
						?>
					</div>
            	</div>
			</div>

			<div class="column col-2 sidebar">                
            <?php //if( (current_user_can('administrator')) || (current_user_can('editor')) ) { ?>
              <!--//  <a href="/create-a-lesson" class="button_alt"><?php if(get_field('new_lesson_btn', 'option')){ the_field('new_lesson_btn', 'option'); } else { ?>New Workspace<?php } ?> <i class="fas fa-plus-circle"></i></a> //-->
            <?php // } ?>
				<a href="/groups" class="button_alt"><?php if(get_field('groups_btn', 'option')){ the_field('groups_btn', 'option'); } else { ?>Groups<?php } ?> <i class="fas fa-chalkboard-teacher"></i></a>
				<?php echo do_shortcode('[ultimatemember form_id="443"]'); ?>
			</div>


			<div class="column col-2 sidebar">
			</div>

		</div>
	</div>
</section>


<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>