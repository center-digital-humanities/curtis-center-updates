<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ): ?>

<section>
<div class="container">
	<div class="row">

		<div class="column col-8 blog">
			<!-- <h1><?php $tax_term = $wp_query->query_vars['tax_name']; ?></h1>	 -->
			<h1><i class="fas fa-chalkboard-teacher"></i> <?php single_term_title(); ?></h1>
		</div>
	</div>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="row">

			<div class="column col-4 sidebar">

			<?php get_sidebar(); ?>
			
		</div>

			<div class="column col-8 blog">
				<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><h4 class="single_post"><i class="fas fa-calendar-alt"></i> <?php echo get_the_date(); ?> by <?php the_author(); ?> </h4></time>
				<?php if ( has_post_thumbnail()) : ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
				<?php endif; ?>
				<?php the_excerpt(); ?>
				<p>
				  This lesson currently has
				  <strong><?php comments_number( 'no responses', 'one response', '% responses' ); ?></strong>.
				</p>
				<p align="right"><a href="<?php the_permalink(); ?>" class="btn">Go to Lesson</a></p>
			</div>

		</div>
	<?php endwhile; ?>
</div>
</section>


<?php else: ?>
<h2>No posts to display</h2>	
<?php endif; ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>