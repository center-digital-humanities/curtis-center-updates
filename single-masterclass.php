<?php
/**
 * The Template for displaying all single posts
 */
?>

<?php 
if ( ( is_user_logged_in() && $current_user->ID == $post->post_author ) ) { // Execute code if user is logged in or user is the author
    acf_form_head();
    wp_deregister_style( 'wp-admin' );
}
?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
	<!-- new versions -->
	
	<div class="entry-content">
                        
		<div class="container">
			<div class="row new-version">
            <?php if( (current_user_can('administrator')) || (current_user_can('editor')) ) { ?>
                <a href="/create-a-lesson" class="button_alt"><?php if(get_field('new_lesson_btn', 'option')){ the_field('new_lesson_btn', 'option'); } else { ?>New Workspace<?php } ?> <i class="fas fa-plus-circle"></i></a>
            <?php } ?>
                <a href="/groups" class="button_alt"><?php if(get_field('groups_btn', 'option')){ the_field('groups_btn', 'option'); } else { ?>Groups<?php } ?> <i class="fas fa-chalkboard-teacher"></i></a>
		
			</div> 
				</div>
						
		<?php 
		// get all masterclasses that are a version of the parent class
		$masterclasses = get_posts(array(
			'post_type' => 'masterclass',
			'orderby' => 'date',
			'order' => 'DESC',
			'meta_query' => array(
				array(
					'key' => 'parent_version', 
					'value' => '"' . get_the_ID() . '"', 
					'compare' => 'LIKE'
				)
			)
		));
		?>
		
		<?php if( $masterclasses ): ?>
			
			<?php foreach( $masterclasses as $masterclass ): ?>
								
				<section class="single-column-block <?php if($noPaddingBottom){ ?> no-padding-bottom <?php } ?>">
					
					<div class="container">
						
						<div class="row">
			
							<div class="column col-8">
								
								<h1 class="masterclass-title"><?php echo get_the_title( $masterclass->ID ); ?></h1>
								<p>by <?php echo get_the_author($masterclass->ID); ?></p>
			
							</div>
			
							<div class="column col-4 text-right">
			
								<h4 class="single_post"><i class="fas fa-calendar-alt"></i> <?php echo get_the_date('F j, Y', $masterclass->ID); ?></h4>
			
							</div>
			
						</div>
	
						<div class="row x-center">
			
							<div class="column col-12 blog">
			
								<?php if( have_rows('course_documents', $masterclass->ID) ): ?>
			
									<?php if ( has_post_thumbnail()) : ?>
										<a href="<?php echo get_the_permalink($masterclass->ID); ?>"><?php echo the_post_thumbnail($masterclass->ID); ?></a>
									<?php endif; ?>
			
									<!-- NEEDS TO LOAD ONLY IF THERE IS A PDF -->
			
									<?php while( have_rows('course_documents', $masterclass->ID) ): the_row(); 
			
									$pdf_doc = get_sub_field('add_document', $masterclass->ID);
									$ext = pathinfo($pdf_doc['url'], PATHINFO_EXTENSION);
									?>
									
										<?php if( $pdf_doc ): ?>
											<?php if ($ext == "pdf") { ?>
												<object data="<?php echo $pdf_doc['url']; ?>" type="application/pdf" width="100%" height="740px"></object>
											<?php } ?>
										<?php endif; ?>
			
									<?php endwhile; ?>
									
								<?php endif; ?>
								
							</div>
							
						</div>
						
					</div>
					<div class="container">
						<div class="row">
							
							<div class="column col-1 sidebar">
							</div>
							
							<div class="column col-10 comments">
						
								<h3><i class="fas fa-chalkboard-teacher"></i> <?php echo get_the_term_list( $masterclass->ID, 'groups', '', ', ' ); ?></h3>
						
								<?php $summary = get_field('brief_lesson_summary', $masterclass->ID); ?>
						
								<div class="course_details">
								<p><?php echo $summary; ?></p>
								</div>
								
								<div class="comments-block"> 
									<?php comments_template( '', true ); ?>
								</div>
							</div>
							
							<div class="column col-1 sidebar">
							</div>
						</div>
					</div>
				</section>
				
				
				
			<?php endforeach; ?>
				
		<?php endif; ?>

	</div>
	
	<!-- new versions end -->
	
	<!-- original -->
	<section class="single-column-block <?php if($noPaddingBottom){ ?> no-padding-bottom <?php } ?>">
		
		<div class="container">
			
			<div class="row">

				<div class="column col-8">
					
					<h1 class="masterclass-title"><?php the_title(); ?></h1>
					<p>by <?php the_author(); ?></p>

				</div>

				<div class="column col-4 text-right">

					<h4 class="single_post"><i class="fas fa-calendar-alt"></i> <?php the_date(); ?></h4>

				</div>

			</div>

			<div class="row x-center">

				<div class="column col-12 blog">

					<?php if( have_rows('course_documents') ): ?>

						<?php if ( has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
						<?php endif; ?>

						<!-- NEEDS TO LOAD ONLY IF THERE IS A PDF -->

						<?php while( have_rows('course_documents') ): the_row(); 

						$pdf_doc = get_sub_field('add_document');
						$ext = pathinfo($pdf_doc['url'], PATHINFO_EXTENSION);
						?>
						
							<?php if( $pdf_doc ): ?>
								<?php if ($ext == "pdf") { ?>
									<object data="<?php echo $pdf_doc['url']; ?>" type="application/pdf" width="100%" height="740px"></object>
								<?php } ?>
							<?php endif; ?>

					<?php endwhile; ?>
					
				</div>
			</div>
		</div>
	
		<div class="container">
			<div class="row">
				<div class="column col-1 sidebar">
				</div>
				<div class="column col-10 comments">
			
					<h3><i class="fas fa-chalkboard-teacher"></i> <?php echo get_the_term_list( $post->ID, 'groups', '', ', ' ); ?></h3>
			
					<?php $summary = get_field('brief_lesson_summary'); ?>
			
					<div class="course_details"><?php echo $summary; ?></div>
			
			
					<?php endif; ?>
					<div class="comments-block"> 
						<?php comments_template( '', true ); ?>
					</div>
			
				</div>
				
				<div class="column col-1 sidebar">
				</div>
			</div>
		</div>
	</section>
	
	
	<?php
	/* Show the edit button to the post author only */
	$current_user = wp_get_current_user(); // Just in case it is not set anywhere else
	if ( ( is_user_logged_in() && $current_user->ID == $post->post_author ) ) { ?>
		<section id="edit-lesson-modal" class="lesson-edit-form" style="background:#fff; overflow:auto;">
			<div class="container">
				<div class="row x-center">
					<div class="column col-10">
			            <div class="acf-edit-post">
			            	<div id="close-button">x</div>
			            	<h2>Edit <?php the_title(); ?></h2>
							<?php  
							acf_form(array(
								'field_groups' => array(468), // Used ID of the field groups here.
								'form' => true,
								'return' => '%post_url%',
								'submit_value' => 'Update Lesson',
								'post_title' => true,
	                			//'post_content' => true,
							));
							?>
			            </div>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	
	<!-- original end -->
	
	<?php endwhile; ?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>