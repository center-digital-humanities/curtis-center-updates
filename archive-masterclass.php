<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<section>
    <div class="container">
        <div class="row">

            <div class="column col-8 blog">
                <h1><?php if(get_field('groups_list_title', 'option')){ the_field('groups_list_title', 'option'); } else { ?>The Collab<?php } ?></h1>
                <h2><i class="fas fa-chalkboard-teacher"></i> <?php if(get_field('gp_sub_title', 'option')) { the_field('gp_sub_title', 'option'); } else { ?>Groups<?php } ?></h2>
                <h3><?php if(get_field('gp_content_area', 'option')){ the_field('gp_content_area', 'option'); } else { ?>Select your group from below. You will only have acccess to lessons in your group.<?php } ?></h3>
            </div>

            <div class="column col-2 sidebar">                
            <?php //if( (current_user_can('administrator')) || (current_user_can('editor')) ) { ?>
              <!--//  <a href="/create-a-lesson" class="button_alt"><?php if(get_field('new_lesson_btn', 'option')){ the_field('new_lesson_btn', 'option'); } else { ?>New Workspace<?php } ?> <i class="fas fa-plus-circle"></i></a> //-->
            <?php // } ?>
                    <?php echo do_shortcode('[ultimatemember form_id="443"]'); ?>
            </div>
        </div>
    </div>
</section>
<section class="resources-block">
    <div class="resources">
        <div class="container">
            <?php
                $taxonomy = 'groups';
                $the_user_levels = rua_get_user_levels(get_current_user_id());
            ?>
            <?php if( (current_user_can('administrator')) && (!empty($the_user_levels)) ) { ?>
                <div class="admin-list row">
                    <?php
                        $meta_array = array('relation'=>'OR');
                        foreach ($the_user_levels as $key => $value) {
                            array_push($meta_array,
                                array(
                                    'key' => 'check_user',
                                    'value' => $value,
                                    'compare' => '='
                                )
                            );
                        }

                        $admin_post_args = array(
                            'hide_empty' => 1, 
                            'parent' =>0,
                            $taxonomy,
                            'meta_query' => $meta_array,
                            'order'				=> 'ASC',
                            'orderby'			=> 'title'

                        );
                    ?>
                    <?php $wcatTerms = get_terms( $taxonomy, array('hide_empty' => 1, 'parent' =>0)); ?>
                    <?php if ( $wcatTerms ) { ?>
                        <?php foreach($wcatTerms as $wcatTerm) : ?>
                             <?php
                                $wsubargs = array(
                                    'hierarchical' => 1,
                                    'hide_empty' => 0,
                                    'parent' => $wcatTerm->term_id,
                                    'taxonomy' => $taxonomy,
                                    'meta_query' => $meta_array,
                                    'order'		 => 'ASC',
                                    'orderby'    => 'meta_value_num'
                                );
                                $wsubcats = get_categories($wsubargs); 
                              ?>
                            <?php if($wsubcats){ ?>
                                <label for="<?php echo $wcatTerm->slug; ?>"><h5><?php echo $wcatTerm->name; ?><?php if($wsubcats){ ?><span class="fas fa-caret-right"></span><?php } ?></h5></label>
                                <input type="checkbox" id="<?php echo $wcatTerm->slug; ?>" />                            
                                  <div class="content">
                                      <div class="resource-list">
                                      <?php foreach ($wsubcats as $wsc): ?>
                                        <?php if ($wsc->name != 'Pending'): ?>
                                            <div class="resource-item">
                                                <div class="module">
                                                    <h3 class="headline"><span style="background-color: #ffc72f ;"></span><?php echo $wsc->name; ?></h3>
                                                        <div class="resource-copy">	
                                                            <?php echo $wsc->description; ?>
                                                        </div>
                                                    <a href="<?php echo get_term_link($wsc->slug, $taxonomy ); ?>">Go to group <span>&rarr;</span></a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                     <?php endforeach; ?>  
                                      </div>
                                  </div>
                            <?php } ?>
                        <?php endforeach; ?>
                    <?php } ?>
                </div>

            <?php } elseif( !empty($the_user_levels) ) { ?>
                <div class="resource-list">					
                    <?php  
                        $meta_array = array('relation'=>'OR');
                        foreach ($the_user_levels as $key => $value) {
                            array_push($meta_array,
                                array(
                                    'key' => 'check_user',
                                    'value' => $value,
                                    'compare' => '='
                                )
                            );
                        }
                        $post_args = array(
                            $taxonomy,
                            'meta_query' => $meta_array,
                            'order'				=> 'ASC',
                            'orderby'			=> 'title'
                        );

                        $terms = get_terms($post_args);
                        if ( $terms  ) :
                    ?>
                        <?php foreach ( $terms as $term ) { ?>
                            <?php if ($term->name != 'Pending'): ?>
                                <div class="resource-item"><div class="module">
                                    <h3 class="headline"><span style="background-color: #ffc72f ;"></span><?php echo $term->name; ?></h3>
                                    <div class="resource-copy">	
                                    <?php echo $term->description; ?>
                                    </div>
                                    <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>">Go to group <span>&rarr;</span></a>
                                </div>
                                </div>
                            <?php endif ?>
                        <?php } ?>					    
                    <?php endif;?>
                </div>
            <?php } else { ?>
                <h4 style="margin-left: 10%;">Sorry you don't access to any groups at this time.<br /> 
                    Please contact The Curtis Center so we can set the Acces for you.<br />
                    Thanks.
                </h4>
            <?php } ?>
        </div>
    </div>
</section>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>